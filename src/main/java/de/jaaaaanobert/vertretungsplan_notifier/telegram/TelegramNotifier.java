package de.jaaaaanobert.vertretungsplan_notifier.telegram;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.User;
import com.pengrad.telegrambot.model.request.InlineKeyboardButton;
import com.pengrad.telegrambot.model.request.InlineKeyboardMarkup;
import com.pengrad.telegrambot.model.request.ParseMode;
import com.pengrad.telegrambot.request.SendMessage;
import de.jaaaaanobert.vertretungsplan_notifier.fileHandler.ConfigFileReader;
import de.jaaaaanobert.vertretungsplan_notifier.fileHandler.FileLoader;
import de.jaaaaanobert.vertretungsplan_notifier.fileWriter.LogFileWriter;
import de.jaaaaanobert.vertretungsplan_notifier.main;
import de.jaaaaanobert.vertretungsplan_notifier.util.ContentPreperation;
import de.jaaaaanobert.vertretungsplan_notifier.util.Data;
import de.jaaaaanobert.vertretungsplan_notifier.util.TextFormatTools;

import java.util.Date;
import java.util.List;

public class TelegramNotifier {

    LogFileWriter logFileWriter = new LogFileWriter();

    private final TelegramBot bot = new TelegramBot( ConfigFileReader.getInstance().getTELEGRAMBOTTOKEN() );

    public void EventListener() {

        bot.setUpdatesListener( list -> {


            if ( list.size() >= 1 ) {
                for ( Update update : list ) {

                    if ( update.message().text() != null ) {

                        final String message = update.message().text();

                        logFileWriter.logging( Data.PREFIXBOTMESSAGE, update.message().chat().firstName()
                                + "-" + update.message().chat().lastName()
                                + " (" + update.message().chat().id() + ") " + "wrote: "
                                + message );

                        if ( message.startsWith( "/broadcast" ) ) {
                            if ( ConfigFileReader.getInstance().getTELEGRAMADMINID().contains( update.message().chat().id() ) ) {
                                if ( message.contains( " " ) )
                                    for ( Long l : new Userinformation().getAllUsers() )
                                        bot.execute( new SendMessage( l, message.split( " ", 2 )[1] ) );
                            } else {
                                bot.execute( new SendMessage( update.message().chat().id(), "Dazu hast keine keine Berechtigung!" ) );
                            }
                        }

                        switch ( message.toLowerCase() ) {

                            case "/start":
                                bot.execute( new SendMessage( update.message().chat().id(),
                                        "Herzlich willkommen beim Vertretungsplan-Bot für die Stufe Q1. \n" +
                                                "Um zukünfig Benachrichtigungen zu erhalten, trage dich bitte in das folgende Dokument ein:\n" +
                                                "https://forms.gle/E56jNzwEuBgVs9mJA \n" +
                                                "Beachte bitte, dass es einige Zeit dauern kann bis deine Anfrage bearbeitet wurde und du Benachrichtungen erhälst! \n" +
                                                "Deine Telegram Chat-ID lautet: *" + update.message().chat().id() + "*" )
                                        .parseMode( ParseMode.Markdown ) );
                                break;

                            case "/getid":
                                bot.execute( new SendMessage( update.message().chat().id(), "Deine Chat-ID ist: " +
                                        "*" + update.message().chat().id() + "*" ).parseMode( ParseMode.Markdown ) );
                                break;

                            case "/update":
                                if ( ConfigFileReader.getInstance().getTELEGRAMADMINID().contains( update.message().chat().id() ) ) {
                                    main main = new main();
                                    main.syncRoutine();
                                    bot.execute( new SendMessage( update.message().chat().id(), "Synchronisation gestartet" ) );
                                } else
                                    bot.execute( new SendMessage( update.message().chat().id(), "Dazu hast keine keine Berechtigung!" ) );

                                break;

                        }
                    }
                }
            }

            return UpdatesListener.CONFIRMED_UPDATES_ALL;
        } );
    }


    public void sendAlert( Integer objectID, FileLoader fl, String searchParameter, int setting ) {

        if ( ConfigFileReader.getInstance().notifierActive() ) {

            //setting 0 = nothing, 1 = new entry,2 = edit, 3 = deleted

            ContentPreperation cp = new ContentPreperation( objectID, fl, searchParameter );

            String courseName = new TextFormatTools().formatCourseName( cp.MessageFileInformation().getCourseName() );
            List<Long> chatIDs = new Userinformation().getAffectedUsers( courseName );

            Date date = new Date();

            int currentYear = date.getYear() + 1900;

            switch ( setting ) {
                case 1:
                    if ( cp.MessageFileInformation().getAbsenceType().equalsIgnoreCase( "entfall" )
                            || cp.MessageFileInformation().getAbsenceType().equalsIgnoreCase( "freisetzung" ) ) {
                        for ( Long l : chatIDs ) {
                            bot.execute( new SendMessage( l, "Information für den: *" + cp.MessageFileDate() + "." + currentYear +
                                    "*, *" + cp.MessageFileInformation().getAbsenceType() + "* für den Kurs: *"
                                    + courseName + "*. Stunde: *" +
                                    cp.MessageFileInformation().getHours() + "*" ).parseMode( ParseMode.Markdown ) );
                        }

                    } else {

                        for ( Long l : chatIDs ) {
                            bot.execute( new SendMessage( l, "Information für den: *" + cp.MessageFileDate() + "." + currentYear +
                                    "*, *" + cp.MessageFileInformation().getAbsenceType() + "* für den Kurs: *"
                                    + courseName + "* in Raum: *" + cp.MessageFileInformation().getRoom() + "*. Stunde: *" +
                                    cp.MessageFileInformation().getHours() + "*" ).parseMode( ParseMode.Markdown ) );
                        }
                    }
                    break;

                case 2:

                    if ( cp.MessageFileInformation().getAbsenceType().equalsIgnoreCase( "entfall" )
                            || cp.MessageFileInformation().getAbsenceType().equalsIgnoreCase( "freisetzung" ) ) {
                        for ( Long l : chatIDs ) {
                            bot.execute( new SendMessage( l, "Änderung am: *" + cp.MessageFileDate() + "." + currentYear +
                                    "*, *" + cp.MessageFileInformation().getAbsenceType() + "* für den Kurs: *"
                                    + courseName + "*. Stunde: *" +
                                    cp.MessageFileInformation().getHours() + "*" ).parseMode( ParseMode.Markdown ) );
                        }

                    } else {

                        for ( Long l : chatIDs ) {
                            bot.execute( new SendMessage( l, "Änderung am: *" + cp.MessageFileDate() + "." + currentYear +
                                    "*, *" + cp.MessageFileInformation().getAbsenceType() + "* für den Kurs: *"
                                    + courseName + "* in Raum: *" + cp.MessageFileInformation().getRoom() + "*. Stunde: *" +
                                    cp.MessageFileInformation().getHours() + "*" ).parseMode( ParseMode.Markdown ) );
                        }
                    }
                    break;


                case 3:

                    if ( cp.MessageFileInformation().getAbsenceType().equalsIgnoreCase( "entfall" ) ||
                            cp.MessageFileInformation().getAbsenceType().equalsIgnoreCase( "freisetzung" ) ) {
                        for ( Long l : chatIDs ) {
                            bot.execute( new SendMessage( l, "Änderung am: *" + cp.MessageFileDate() + "." + currentYear +
                                    "*, *" + cp.MessageFileInformation().getAbsenceType() + "* für den Kurs: *"
                                    + courseName + "*. Stunde: *" +
                                    cp.MessageFileInformation().getHours() +
                                    " Diese Information ist nicht mehr gültig, es findet regulärer Unterricht statt!*" )
                                    .parseMode( ParseMode.Markdown ) );
                        }

                    } else {

                        for ( Long l : chatIDs ) {
                            bot.execute( new SendMessage( l, "Änderung am: *" + cp.MessageFileDate() + "." + currentYear +
                                    "*, *" + cp.MessageFileInformation().getAbsenceType() + "* für den Kurs: *"
                                    + courseName + "* in Raum: *" + cp.MessageFileInformation().getRoom() + "*. Stunde: *" +
                                    cp.MessageFileInformation().getHours() +
                                    " Diese Information ist nicht mehr gültig, es findet regulärer Unterricht statt!*" )
                                    .parseMode( ParseMode.Markdown ) );
                        }


                    }

                    break;

            }


        } else {
            System.out.println( Data.PREFIXWARNING + " " + Data.NOTIFIERDISABLED );
            new LogFileWriter().logging( Data.PREFIXWARNING,
                    Data.NOTIFIERDISABLEDWARNING );
        }
    }


}


