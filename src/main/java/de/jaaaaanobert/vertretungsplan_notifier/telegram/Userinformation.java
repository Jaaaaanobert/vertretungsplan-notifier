package de.jaaaaanobert.vertretungsplan_notifier.telegram;

import de.jaaaaanobert.vertretungsplan_notifier.database.DatabaseConnect;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Userinformation {

    public List<Long> getAffectedUsers( String courseName ) {

        List<Long> userIDs = new ArrayList();

        try {

            Statement st = DatabaseConnect.getInstance().con.createStatement();

            ResultSet rs = st.executeQuery( "SELECT * from Botuser2022 WHERE `" + courseName + "` = 1" );

            while ( rs.next() ) {
                userIDs.add( rs.getLong( "telegramid" ) );
            }

            return userIDs;

        } catch ( SQLException e ) {
            e.printStackTrace();

            return null;
        }


    }

    public List<Long> getAllUsers() {
        List<Long> userIDs = new ArrayList<>();
        try {
            Statement st = DatabaseConnect.getInstance().con.createStatement();
            ResultSet rs = st.executeQuery( "SELECT * from Botuser2022" );

            while ( rs.next() ) {
                userIDs.add( rs.getLong( "telegramid" ) );
            }
        } catch ( SQLException e ) {
            e.printStackTrace();
        }

        return userIDs;
    }
}
