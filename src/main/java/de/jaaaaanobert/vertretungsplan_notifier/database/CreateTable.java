package de.jaaaaanobert.vertretungsplan_notifier.database;


import de.jaaaaanobert.vertretungsplan_notifier.fileHandler.GetFileInformation;
import de.jaaaaanobert.vertretungsplan_notifier.util.TextFormatTools;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CreateTable {

    String docString;
    String searchParameter;

    public CreateTable( String docString, String searchParameter ) {
        this.docString = docString;
        this.searchParameter = searchParameter;
    }

    public boolean isCreated() throws SQLException {

        Statement st = DatabaseConnect.getInstance().con.createStatement();
        ResultSet rs = st.executeQuery( "SHOW TABLES" );
        String date = new TextFormatTools().replaceDottoMinus( new GetFileInformation( docString ).getAbsenceDate() );
        while ( rs.next() ) {
            if ( rs.getString( 1 ).equals( date + "_" + searchParameter ) ) {
                return true;
            }
        }
        return false;


    }

    public void createTable() throws SQLException {
        String date = new TextFormatTools().replaceDottoMinus( new GetFileInformation( docString ).getAbsenceDate() );
        Statement st = DatabaseConnect.getInstance().con.createStatement();
        st.execute( "CREATE TABLE `EBGS-Vertretungsplan`." +
                "`" + date + "_" + searchParameter +
                "` ( `id` INT NOT NULL AUTO_INCREMENT , `classname` VARCHAR(30) NOT NULL , " +
                "`hours` VARCHAR(30) NOT NULL , `coursename` VARCHAR(30) NOT NULL , " +
                "`room` VARCHAR(30) NOT NULL , `type` VARCHAR(30) NOT NULL , " +
                "`notification` BOOLEAN NOT NULL DEFAULT FALSE , " +
                "PRIMARY KEY (`id`)) ENGINE = InnoDB;" );
        st.close();
    }
}
