package de.jaaaaanobert.vertretungsplan_notifier.database;

import de.jaaaaanobert.vertretungsplan_notifier.fileHandler.FileLoader;
import de.jaaaaanobert.vertretungsplan_notifier.fileHandler.FileSearcher;
import de.jaaaaanobert.vertretungsplan_notifier.fileHandler.GetFileData;
import de.jaaaaanobert.vertretungsplan_notifier.fileHandler.GetFileInformation;
import de.jaaaaanobert.vertretungsplan_notifier.fileWriter.LogFileWriter;
import de.jaaaaanobert.vertretungsplan_notifier.telegram.TelegramNotifier;
import de.jaaaaanobert.vertretungsplan_notifier.util.Data;
import de.jaaaaanobert.vertretungsplan_notifier.util.TextFormatTools;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SynchroniseEntries {

    private FileLoader fileLoader;
    private String searchParameter;

    public SynchroniseEntries( FileLoader fileLoader, String searchParameter ) {
        this.fileLoader = fileLoader;
        this.searchParameter = searchParameter;
    }

    public void DataBaseComparison() throws SQLException {
        int editcounter = 0, addcounter = 0, deletecounter = 0;

        CreateTable createTable = new CreateTable( fileLoader.getFileText(), searchParameter );
        if ( !createTable.isCreated() ) {
            createTable.createTable();
        }

        GetFileInformation getFileInformation = new GetFileInformation( fileLoader.getFileText() );
        String fileDate = new TextFormatTools().replaceDottoMinus( getFileInformation.getAbsenceDate() );

        HashMap<String, Integer> dbSchedule = getDBSchedule();
        HashMap<String, Integer> dbScheduleIdentifier = getDBScheduleIdentifier();
        List<String> currentSchedule = getCurrentSchedule();
        List<String> currentScheduleIdentifier = getCurrentScheduleIdentifier();
        HashMap<String, String> currentScheduleIdentifierMap = getcurrentScheduleIdentifierMap();
        DatabaseFunctions databaseFunctions = new DatabaseFunctions();

        LogFileWriter logFileWriter = new LogFileWriter();

        System.out.println( "Data pulled successfully in instance " + searchParameter + " " + fileDate );
        logFileWriter.logging( Data.PREFIXSYSTEM,
                "Data pulled successfully in instance " + searchParameter + " " + fileDate );

        for ( String s : currentSchedule ) {
            if ( !dbSchedule.containsKey( s ) ) {
                if ( dbScheduleIdentifier.containsKey( currentScheduleIdentifierMap.get( s ) ) ) {
                    //edit entry
                    databaseFunctions.editEntry( dbScheduleIdentifier.get( currentScheduleIdentifierMap.get( s ) ),
                            currentScheduleIdentifier.indexOf( currentScheduleIdentifierMap.get( s ) ), fileLoader, searchParameter );
                    editcounter++;

                    new TelegramNotifier().sendAlert( currentSchedule.indexOf( s ), fileLoader, searchParameter, 2 );
                } else {
                    //add entry
                    databaseFunctions.addEntry( currentSchedule.indexOf( s ), fileLoader, searchParameter );
                    addcounter++;

                    new TelegramNotifier().sendAlert( currentSchedule.indexOf( s ), fileLoader, searchParameter, 1 );

                }

            }
        }


        List<String> dbScheduleList = new ArrayList<>( getDBSchedule().keySet() );

        for ( String s : dbScheduleList ) {
            if ( !getCurrentSchedule().contains( s ) ) {
                //delete entry
                databaseFunctions.deleteEntry( dbSchedule.get( s ), fileLoader, searchParameter );
                deletecounter++;

                //new TelegramNotifier().sendAlert( dbScheduleList.indexOf( s ), fileLoader, searchParameter, 3 );
                //removed due to a bug
            }
        }

        System.out.println( "\n" + addcounter + " Entrys added" );
        System.out.println( editcounter + " Entrys edited" );
        System.out.println( deletecounter + " Entrys deleted" );
        System.out.println( "In instance: " + searchParameter + " " + fileDate );
        System.out.println( "\n" );

        logFileWriter.logging( Data.PREFIXSYSTEM, addcounter + " Entrys added" );
        logFileWriter.logging( Data.PREFIXSYSTEM, editcounter + " Entrys edited" );
        logFileWriter.logging( Data.PREFIXSYSTEM, deletecounter + " Entrys deleted" );
        logFileWriter.logging( Data.PREFIXSYSTEM, "In instance: " + searchParameter + " " + fileDate + "\n" );
    }

    public HashMap<String, Integer> getDBSchedule() throws SQLException { //pulls current Schedule from Database


        GetFileInformation getFileInformation = new GetFileInformation( fileLoader.getFileText() );

        HashMap<String, Integer> DBSchedule = new HashMap<>();

        Statement st = DatabaseConnect.getInstance().con.createStatement();


        String fileDate = new TextFormatTools().replaceDottoMinus( getFileInformation.getAbsenceDate() );

        ResultSet rs = st.executeQuery( "SELECT * from `" + fileDate + "_" + searchParameter + "`" );
        String dataset;

        while ( rs.next() ) {
            dataset = rs.getString( "classname" ) + " "
                    + rs.getString( "hours" ) + " "
                    + rs.getString( "coursename" ) + " " + rs.getString( "room" ) + " "
                    + rs.getString( "type" );
            DBSchedule.put( dataset, rs.getInt( "id" ) );
        }

        st.close();
        rs.close();

        return DBSchedule;
    }

    public List<String> getCurrentSchedule() { //pulls current Schedule from Website
        FileSearcher fs = new FileSearcher( searchParameter, fileLoader );

        List<String> currentSchedule = new ArrayList<>();

        String dataset;

        GetFileData gfd;

        for ( int i = 0; i < fs.getResults().size(); i++ ) {
            gfd = fs.getResults().get( i );
            dataset = gfd.getClassName() + " " + gfd.getHours() + " "
                    + gfd.getCourseName() + " " + gfd.getRoom() + " "
                    + gfd.getAbsenceType();
            currentSchedule.add( dataset );
        }

        return currentSchedule;
    }

    public List<String> getCurrentScheduleIdentifier() { //Pulls short schedule from Database (only class, hours and course)
        FileSearcher fs = new FileSearcher( searchParameter, fileLoader );

        List<String> currentSchedule = new ArrayList<>();

        String dataset;

        GetFileData gfd;

        for ( int i = 0; i < fs.getResults().size(); i++ ) {
            gfd = fs.getResults().get( i );
            dataset = gfd.getClassName() + " " + gfd.getHours() + " "
                    + gfd.getCourseName();
            currentSchedule.add( dataset );
        }


        return currentSchedule;
    }

    public HashMap<String, Integer> getDBScheduleIdentifier() throws SQLException { //Pulls short schedule from Website (only class, hours and course)
        GetFileInformation getFileInformation = new GetFileInformation( fileLoader.getFileText() );

        HashMap<String, Integer> DBSchedule = new HashMap<>();

        Statement st = DatabaseConnect.getInstance().con.createStatement();

        TextFormatTools tft = new TextFormatTools();

        String fileDate = new TextFormatTools().replaceDottoMinus( getFileInformation.getAbsenceDate() );
        fileDate = tft.replaceDottoMinus( fileDate );

        ResultSet rs = st.executeQuery( "SELECT * from `" + fileDate + "_" + searchParameter + "`" );
        String dataset;

        while ( rs.next() ) {
            dataset = rs.getString( "classname" ) + " " + rs.getString( "hours" ) + " "
                    + rs.getString( "coursename" );
            DBSchedule.put( dataset, rs.getInt( "id" ) );
        }

        st.close();
        rs.close();

        return DBSchedule;
    }

    public HashMap<String, String> getcurrentScheduleIdentifierMap() { //pulls map with Identifier
        FileSearcher fs = new FileSearcher( searchParameter, fileLoader );

        HashMap<String, String> getcurrentScheduleIdentifierMap = new HashMap<>();

        String dataset, key;

        GetFileData gfd;

        for ( int i = 0; i < fs.getResults().size(); i++ ) {
            gfd = fs.getResults().get( i );

            key = gfd.getClassName() + " " + gfd.getHours() + " "
                    + gfd.getCourseName() + " " + gfd.getRoom() + " "
                    + gfd.getAbsenceType();

            dataset = gfd.getClassName() + " " + gfd.getHours() + " "
                    + gfd.getCourseName();

            getcurrentScheduleIdentifierMap.put( key, dataset );
        }


        return getcurrentScheduleIdentifierMap;
    }

    public boolean dbContainsIdentifier( HashMap<String, Integer> dbScheduleIdentifier, List<String> currentScheduleIdentifier ) {
        for ( String s : currentScheduleIdentifier ) {
            if ( dbScheduleIdentifier.containsKey( s ) )
                return true;
        }
        return false;
    }
}
