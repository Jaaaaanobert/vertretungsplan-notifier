package de.jaaaaanobert.vertretungsplan_notifier.database;

import de.jaaaaanobert.vertretungsplan_notifier.fileHandler.FileLoader;
import de.jaaaaanobert.vertretungsplan_notifier.fileHandler.FileSearcher;
import de.jaaaaanobert.vertretungsplan_notifier.fileHandler.GetFileData;
import de.jaaaaanobert.vertretungsplan_notifier.fileHandler.GetFileInformation;
import de.jaaaaanobert.vertretungsplan_notifier.util.TextFormatTools;

import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseFunctions {

    public void editEntry( Integer dbID, Integer objectID, FileLoader fl, String searchParameter ) throws SQLException {
        String fileText = fl.getFileText();
        GetFileInformation getFileInformation = new GetFileInformation( fileText );
        TextFormatTools tft = new TextFormatTools();
        String fileDate = tft.replaceDottoMinus( getFileInformation.getAbsenceDate() );

        FileSearcher fileSearcher = new FileSearcher( searchParameter, fl );
        GetFileData getFileData = fileSearcher.getResults().get( objectID );

        Statement st = DatabaseConnect.getInstance().con.createStatement();
        st.execute( "UPDATE `" + fileDate + "_" + searchParameter +
                "` SET `classname` = '" + getFileData.getClassName() + "', `hours` = '" + getFileData.getHours() +
                "', `coursename` = '" + getFileData.getCourseName() + "', `room` = '" + getFileData.getRoom() +
                "', `type` = '" + getFileData.getAbsenceType() + "' WHERE ID=" + dbID );
        st.close();
    }

    public void addEntry( Integer objectID, FileLoader fl, String searchParameter ) throws SQLException {

        String fileText = fl.getFileText();
        GetFileInformation getFileInformation = new GetFileInformation( fileText );
        TextFormatTools tft = new TextFormatTools();
        String fileDate = tft.replaceDottoMinus( getFileInformation.getAbsenceDate() );


        FileSearcher fileSearcher = new FileSearcher( searchParameter, fl );
        GetFileData getFileData = fileSearcher.getResults().get( objectID );

        Statement st = DatabaseConnect.getInstance().con.createStatement();
        st.execute( "INSERT INTO `" + fileDate + "_" + searchParameter + "` (`classname`, `hours`, `coursename`, `room`, `type`) " +
                "VALUES ('" + getFileData.getClassName() + "', '" + getFileData.getHours() + "', '" + getFileData.getCourseName() +
                "', '" + getFileData.getRoom() + "', '" + getFileData.getAbsenceType() + "')" );
        st.close();
    }

    public void deleteEntry( Integer id, FileLoader fl, String searchParameter ) throws SQLException {
        GetFileInformation getFileInformation = new GetFileInformation( fl.getFileText() );
        TextFormatTools tft = new TextFormatTools();
        String fileDate = tft.replaceDottoMinus( getFileInformation.getAbsenceDate() );
        Statement st = DatabaseConnect.getInstance().con.createStatement();
        st.execute( "DELETE FROM `" + fileDate + "_" + searchParameter + "` WHERE ID=" + id );
        st.close();
    }
}
