package de.jaaaaanobert.vertretungsplan_notifier.database;

import de.jaaaaanobert.vertretungsplan_notifier.fileHandler.ConfigFileReader;
import de.jaaaaanobert.vertretungsplan_notifier.fileWriter.LogFileWriter;
import de.jaaaaanobert.vertretungsplan_notifier.util.Data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnect {

    public Connection con = null;

    private DatabaseConnect() {

        LogFileWriter logFileWriter = new LogFileWriter();

        logFileWriter.logging( Data.PREFIXSTARTUP, "Connecting with Database..." );

        System.out.println( Data.PREFIXSTARTUP +  " Connecting with Database..." );
        if ( !isConnected() ) {

            ConfigFileReader configFileReader = ConfigFileReader.getInstance();

            //String url = "jdbc:mysql://192.168.30.47:3306/EBGS-Vertretungsplan?autoReconnect=true&serverTimezone=Europe/Amsterdam&amp";
            try {
                con = DriverManager.getConnection( configFileReader.getDBURL(),
                        configFileReader.getDBUSERNAME(),
                        configFileReader.getDBPASSWORD() );

            } catch ( SQLException e ) {
                e.printStackTrace();
            }
            if ( isConnected() ) {
                System.out.println( Data.PREFIXSTARTUP + " Connected successfully with Database!" );
                logFileWriter.logging( Data.PREFIXSTARTUP, "Connected successfully with Database!" );
            }
        }
    }

    private static final DatabaseConnect databaseConnect = new DatabaseConnect();

    public static DatabaseConnect getInstance() {
        return databaseConnect;
    }

    private boolean isConnected() {
        return con != null;
    }
}
