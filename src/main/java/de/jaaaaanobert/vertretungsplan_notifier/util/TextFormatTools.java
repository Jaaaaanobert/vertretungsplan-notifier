package de.jaaaaanobert.vertretungsplan_notifier.util;

import java.util.Calendar;

public class TextFormatTools {

    public String formatCourseName( String arg ) {

        String result = arg;

        result = result.replaceFirst( " ", "-" );
        result = result.replaceAll( " ", "" );

        return result;
    }

    public String removeAllSpaces( String arg ) {
        arg = arg.replace( " ", "" );
        return arg;
    }

    public String formatRoomName( String arg ) {
        if ( arg.equals( "-" ) || arg.equals( "--" ) || arg.equals( "---" ) | arg.equals( "----" ) )
            arg = "/";
        return arg;
    }

    public String replaceDottoMinus( String arg ) {
        return arg.replaceAll( "\\.", "-" );
    }

    public String formatAbsenceDate( String date ) {
        int year = Calendar.getInstance().get( Calendar.YEAR );

        return date + "." + year;
    }

    public String formatTableNameDate( String arg ) {
        String fileDate = formatAbsenceDate( arg );
        fileDate = replaceDottoMinus( fileDate );

        return fileDate;
    }
}
