package de.jaaaaanobert.vertretungsplan_notifier.util;


public class Data {

    //messages
    public static final String PREFIXSYSTEM = "[System]";
    public static final String PREFIXBOTMESSAGE = "[Bot-Message]";
    public static final String PREFIXSTARTUP = "[Startup]";
    public static final String PREFIXWARNING = "[Warning]";

    public static final String NOTIFIERDISABLED = "The notifier is currently disabled in the config-file";
    public static final String NOTIFIERDISABLEDWARNING = "Tried to send a notification although they are disabled in the config!";
    public static final String NOTIFIERENABLED = "The notifier is enabled";
}
