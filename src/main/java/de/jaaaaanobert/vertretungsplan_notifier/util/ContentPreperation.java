package de.jaaaaanobert.vertretungsplan_notifier.util;

import de.jaaaaanobert.vertretungsplan_notifier.fileHandler.FileLoader;
import de.jaaaaanobert.vertretungsplan_notifier.fileHandler.FileSearcher;
import de.jaaaaanobert.vertretungsplan_notifier.fileHandler.GetFileData;
import de.jaaaaanobert.vertretungsplan_notifier.fileHandler.GetFileInformation;

public class ContentPreperation {

    //class in use to format Telegram-Messages and the content

    private int objectID;
    private FileLoader fl;
    private String searchParameter;

    public ContentPreperation( Integer objectID, FileLoader fl, String searchParamter ) {
        this.objectID = objectID;
        this.fl = fl;
        this.searchParameter = searchParamter;
    }

    public GetFileData MessageFileInformation() {
        FileSearcher fileSearcher = new FileSearcher( searchParameter, fl );

        return fileSearcher.getResults().get( objectID );
    }

    public String MessageFileDate() {

        GetFileInformation getFileInformation = new GetFileInformation( fl.getFileText() );

        return getFileInformation.getAbsenceDate();
    }
}
