package de.jaaaaanobert.vertretungsplan_notifier.util;

import de.jaaaaanobert.vertretungsplan_notifier.fileHandler.FileLoader;
import de.jaaaaanobert.vertretungsplan_notifier.fileHandler.GetFileInformation;

public class Methods {

    public boolean doubleInstanceName( FileLoader firstFile, FileLoader secondFile ) {
        GetFileInformation firstFileInformation = new GetFileInformation( firstFile.getFileText() );
        GetFileInformation secondFileInformation = new GetFileInformation( secondFile.getFileText() );

        TextFormatTools tft = new TextFormatTools();

        return tft.replaceDottoMinus( firstFileInformation.getAbsenceDate() )
                .equals( tft.replaceDottoMinus( secondFileInformation.getAbsenceDate() ) );

    }
}
