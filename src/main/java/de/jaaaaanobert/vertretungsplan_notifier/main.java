package de.jaaaaanobert.vertretungsplan_notifier;

import java.util.Timer;
import java.util.TimerTask;

import de.jaaaaanobert.vertretungsplan_notifier.database.DatabaseConnect;
import de.jaaaaanobert.vertretungsplan_notifier.database.SynchroniseEntries;
import de.jaaaaanobert.vertretungsplan_notifier.fileHandler.ConfigFileReader;
import de.jaaaaanobert.vertretungsplan_notifier.fileHandler.FileLoader;
import de.jaaaaanobert.vertretungsplan_notifier.telegram.TelegramNotifier;
import de.jaaaaanobert.vertretungsplan_notifier.util.Data;
import de.jaaaaanobert.vertretungsplan_notifier.util.Methods;

public class main {
    public static void main( String[] args ) {
        System.out.println( Data.PREFIXSTARTUP + " " + "Initialisation..." );
        DatabaseConnect.getInstance();
        new TelegramNotifier().EventListener();

        ConfigFileReader configFileReader = ConfigFileReader.getInstance();

        if ( configFileReader.notifierActive() ) {
            System.out.println( Data.PREFIXSTARTUP + " " + Data.NOTIFIERENABLED );
        } else System.out.println( Data.PREFIXSTARTUP + " " + Data.NOTIFIERDISABLED );

        Timer timer = new Timer();
        timer.schedule( new TimerTask() {
            @Override
            public void run() {
                try {

                    new main().syncRoutine();

                } catch ( Exception e ) {
                    e.printStackTrace();
                }
            }
        }, 0, 1000 * 60 * 20 );

    }

    public void syncRoutine() {

        try {

            ConfigFileReader configFileReader = ConfigFileReader.getInstance();

            FileLoader todayFile = new FileLoader(
                    configFileReader.getFIRSTFILEURL() );
            FileLoader tomorrowFile = new FileLoader(
                    configFileReader.getSECONDFILEURL() );


            for ( String s : configFileReader.getSEARCHPARAMETER() ) {

                new SynchroniseEntries( todayFile, s ).DataBaseComparison();

                if ( !new Methods().doubleInstanceName( todayFile, tomorrowFile ) ) {

                    new SynchroniseEntries( tomorrowFile, s ).DataBaseComparison();

                }
            }
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

}
