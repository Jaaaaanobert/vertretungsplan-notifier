package de.jaaaaanobert.vertretungsplan_notifier.fileHandler;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GetFileInformation {
    private String docString;

    public GetFileInformation( String docString ) {
        this.docString = docString;
    }

    public String getFileTime() {
        String arg = docString;
        arg = arg.split( "\n" )[5].trim();
        Pattern pattern = Pattern.compile( "[0-9]{1,2}.[0-9]{1,2}.[0-9]{1,4}[\\s \\t]*([0-9]{1,2}:[0-9]{1,2}).*" );
        Matcher matcher = pattern.matcher( arg );
        matcher.find();
        return matcher.group( 1 );
    }

    public String getFileDate() {
        String arg = docString;
        arg = arg.split( "\n" )[5].trim();
        Pattern pattern = Pattern.compile( "([0-9]{1,2}.[0-9]{1,2}.[0-9]{1,4}).*" );
        Matcher matcher = pattern.matcher( arg );
        matcher.find();
        return matcher.group( 1 );
    }

    public String getAbsenceDate() {
        Pattern pattern = Pattern.compile( "[\\s \\t]([0-9]{1,2}\\.[0-9]{1,2}).*" );
        Matcher matcher = pattern.matcher( docString );
        matcher.find();
        matcher.find();
        matcher.find();
        return matcher.group( 1 );
    }

}
