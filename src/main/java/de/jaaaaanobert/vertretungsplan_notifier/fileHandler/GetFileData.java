package de.jaaaaanobert.vertretungsplan_notifier.fileHandler;

import de.jaaaaanobert.vertretungsplan_notifier.util.TextFormatTools;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GetFileData {


    private String fileString;

    public GetFileData( String fileString ) {
        this.fileString = fileString;
    }

    public String getHours() {
        Pattern pattern = Pattern.compile( "[\\s \\t]([0-9]{1,2}[\\s \\t]-[\\s \\t][0-9]{1,2}|[0-9]{1,2}).*" );
        Matcher matcher;

        matcher = pattern.matcher( fileString );
        matcher.find();
        return new TextFormatTools().removeAllSpaces( matcher.group( 1 ) );


    }

    public String getCourseName() {
        Pattern pattern = Pattern.compile( "[\\s \\t]([0-9]{1,2}[\\s \\t]-[\\s \\t][0-9]{1,2}|[0-9]{1,2})[\\s \\t]*" +
                "([A-Z[1-9]]{1,6}[\\s \\t]*[A-Z[1-9]]{1,2})[\\s \\t]*.*" );
        Matcher matcher = pattern.matcher( fileString );
        matcher.find();


        return new TextFormatTools().formatCourseName( matcher.group( 2 ) );
    }

    public String getClassName() {
        Pattern pattern = Pattern.compile( "([A-Z[0-9]]{1,6}).*" );
        Matcher matcher = pattern.matcher( fileString );
        matcher.find();
        return matcher.group( 1 );
    }

    public String getRoom() {
        Pattern pattern = Pattern.compile( "[\\s \\t]([0-9]{1,2}[\\s \\t]-[\\s \\t][0-9]{1,2}|[0-9]{1,2})[\\s \\t]*" +
                "([A-Z[1-9]]{1,6}[\\s \\t]*[A-Z[1-9]]{1,2})[\\s \\t]*" +
                "([-]{3}|[A-Z[-][0-9]]{1,10})[\\s \\t]*.*" );
        Matcher matcher = pattern.matcher( fileString );
        matcher.find();
        return new TextFormatTools().formatRoomName( matcher.group( 3 ) );
    }

    public String getAbsenceType() {
        Pattern pattern = Pattern.compile( "[\\s \\t]([0-9]{1,2}[\\s \\t]-[\\s \\t][0-9]{1,2}|[0-9]{1,2})[\\s \\t]*" +
                "([A-Z[1-9]]{1,6}[\\s \\t]*[A-Z[1-9]]{1,2})[\\s \\t]*" +
                "([-]{3}|[A-Z[-][0-9]]{1,10})[\\s \\t]*(.*)" );
        Matcher matcher = pattern.matcher( fileString );
        matcher.find();

        return matcher.group( 4 );
    }

}
