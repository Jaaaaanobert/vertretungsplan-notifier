package de.jaaaaanobert.vertretungsplan_notifier.fileHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileSearcher {

    private List<String> results;

    public FileSearcher( String searchParameter, FileLoader fileString ) {

        this.results = new ArrayList<>();
        String regex = "[0-9]{1,2}.[0-9]{1,2}.[\\s \\t](" + searchParameter +
                "[\\s \\t]([0-9]{1,2}[\\s \\t]-[\\s \\t][0-9]{1,2}|[0-9]{1,2}).*)";
        Pattern pattern = Pattern.compile( regex );
        Matcher matcher = pattern.matcher( fileString.getFileText() );

        while ( matcher.find() )
            this.results.add( matcher.group( 1 ) );
    }

    public List<GetFileData> getResults() {
        List<GetFileData> getResults = new ArrayList<>();
        for ( String o : results )
            getResults.add( new GetFileData( o ) );

        return getResults;
    }
}
