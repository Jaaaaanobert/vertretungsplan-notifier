package de.jaaaaanobert.vertretungsplan_notifier.fileHandler;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.*;
import java.net.URL;

public class FileLoader {

    private String fileText;

    public FileLoader( File file ) throws IOException {

        InputStream is = new FileInputStream( file );
        PDDocument doc = PDDocument.load( is );
        PDFTextStripper stripper = new PDFTextStripper();
        fileText = stripper.getText( doc );
        is.close();
        doc.close();
    }

    public FileLoader( String fileURL ) throws IOException {
        URL url = new URL( fileURL );
        InputStream is = url.openStream();
        PDDocument doc = PDDocument.load( is );
        PDFTextStripper stripper = new PDFTextStripper();
        fileText = stripper.getText( doc );
        is.close();
        doc.close();
    }

    public String getFileText() {

        return fileText;
    }
}
