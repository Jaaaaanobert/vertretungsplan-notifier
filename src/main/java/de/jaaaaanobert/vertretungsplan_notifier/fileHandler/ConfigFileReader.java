package de.jaaaaanobert.vertretungsplan_notifier.fileHandler;

import kotlin.collections.ArrayDeque;

import java.io.*;
import java.util.*;

public class ConfigFileReader {

    private String DBUSERNAME;
    private String DBPASSWORD;
    private String DBURL;

    private String FIRSTFILEURL;
    private String SECONDFILEURL;

    private String TELEGRAMBOTTOKEN;
    private List<Long> TELEGRAMADMINID = new ArrayList<>();
    private String NOTIFIERACTIVE;

    private List<String> SEARCHPARAMETER = new ArrayList<>();

    public String getTELEGRAMBOTTOKEN() {
        return TELEGRAMBOTTOKEN;
    }

    public List<Long> getTELEGRAMADMINID() {
        return TELEGRAMADMINID;
    }

    public String getDBUSERNAME() {
        return DBUSERNAME;
    }

    public String getDBPASSWORD() {
        return DBPASSWORD;
    }

    public String getDBURL() {
        return DBURL;
    }

    public String getFIRSTFILEURL() {
        return FIRSTFILEURL;
    }

    public String getSECONDFILEURL() {
        return SECONDFILEURL;
    }

    public List<String> getSEARCHPARAMETER() {
        return SEARCHPARAMETER;
    }

    public boolean notifierActive() {
        return NOTIFIERACTIVE.equals( "true" );
    }


    private ConfigFileReader() {
        Properties config = new Properties();

        try {
            config.load( new BufferedInputStream( new FileInputStream( "config/config.properties" ) ) );

            HashMap<String, String> configMap = new HashMap<>();

            DBUSERNAME = config.getProperty( "db.username" );
            DBPASSWORD = config.getProperty( "db.password" );
            DBURL = config.getProperty( "db.url" );

            FIRSTFILEURL = config.getProperty( "url.firstfile" );
            SECONDFILEURL = config.getProperty( "url.secondfile" );

            String[] readSearchParameter = config.getProperty( "searchparameter" ).split( ";" );

            SEARCHPARAMETER = Arrays.asList( readSearchParameter.clone() );

            TELEGRAMBOTTOKEN = config.getProperty( "telegram.bottoken" );

            String[] readAdminID = config.getProperty( "telegram.adminid" ).split( ";" );

            for ( String s : readAdminID ) {
                TELEGRAMADMINID.add( Long.parseLong( s ) );
            }


            NOTIFIERACTIVE = config.getProperty( "telegram.sendnotifications" );

        } catch ( FileNotFoundException e ) {
            e.printStackTrace();
            System.exit( 10 );
            System.out.println( "Could not read config-file. Exiting program..." );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    private static final ConfigFileReader configFileReader = new ConfigFileReader();

    public static ConfigFileReader getInstance() {
        return configFileReader;
    }


}
