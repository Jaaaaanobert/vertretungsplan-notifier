package de.jaaaaanobert.vertretungsplan_notifier.fileWriter;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LogFileWriter {

    public void logging( String prefix, String message ) {


        SimpleDateFormat simpleDateFormat = new SimpleDateFormat( "yyyy-MM-dd" );
        Date date = new Date( System.currentTimeMillis() );
        String formattedDate = simpleDateFormat.format( date );

        Time time = new Time( System.currentTimeMillis() );

        try {

            FileOutputStream fos = new FileOutputStream( "log/" + formattedDate + ".log", true );

            String content = prefix + " " + time + ": " + message + "\n";

            fos.write( content.getBytes() );

            fos.close();

        } catch ( IOException e ) {
            e.printStackTrace();
            System.out.println( "Error while creating logfile" );
        }


    }
}
